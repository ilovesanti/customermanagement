package com.its333.mycustomer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class DetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		Intent data = getIntent();
		//Check the activity is started with extra
		((TextView)findViewById(R.id.name)).setText(data.getStringExtra("ct_name"));
		((TextView)findViewById(R.id.phone)).setText(data.getStringExtra("ct_phone"));
		((TextView)findViewById(R.id.address)).setText(data.getStringExtra("ct_address"));
		((TextView)findViewById(R.id.email)).setText(data.getStringExtra("ct_email"));
		((TextView)findViewById(R.id.info)).setText(data.getStringExtra("ct_info"));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.detail, menu);
		return true;
	}

}
