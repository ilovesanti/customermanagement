package com.its333.mycustomer;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Assignment extends Activity implements OnClickListener {

	DBHelper dbHelper;
	SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assignment);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		
		
		
		
		Button b1 = (Button)findViewById(R.id.button1);
		b1.setOnClickListener(this);
	} 

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.assignment, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		 EditText editText1 = (EditText) findViewById(R.id.editText1);
		 EditText editText2 = (EditText) findViewById(R.id.editText2);
		 EditText editText3 = (EditText) findViewById(R.id.editText3);
		 
		 String e1 = editText1.getText().toString();
		 String e2 = editText2.getText().toString();
		 String e3 = editText3.getText().toString();
		 
		 ContentValues va = new ContentValues();
		 va.put("ct_name", e1);
		 va.put("ct_duedate", e2);
		 va.put("ct_description", e3);
		 
		 db.insert("contacts", null,va);
		 
		 Toast.makeText(Assignment.this, "Insert to Database " + e1+"-"+e2+"-"+e3, Toast.LENGTH_LONG).show();
		 
	}

}
