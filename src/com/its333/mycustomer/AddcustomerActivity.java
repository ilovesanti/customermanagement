package com.its333.mycustomer;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddcustomerActivity extends Activity {
	long id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcustomer);
		Intent i = this.getIntent();
		if (i.hasExtra("id")){
			//Editing
			//Load the Extra
			String name = i.getStringExtra("ct_name");
			String phone = i.getStringExtra("ct_phone");
			String email = i.getStringExtra("ct_email");
			int type = i.getIntExtra("ct_type",0);
			
			Log.d("TEST",type+"");
			System.out.print(type+"");
			String address = i.getStringExtra("ct_address");			
			String info = i.getStringExtra("ct_info");
	
			id = i.getLongExtra("id", -1);
			//int position = i.getIntExtra("pos", -1);
			//Set them to the widgets
			EditText etName = (EditText)findViewById(R.id.etName);
			etName.setText(name);
			
			((EditText)findViewById(R.id.etPhone)).setText(phone);
			((EditText)findViewById(R.id.etEmail)).setText(email);
			((EditText)findViewById(R.id.editText1)).setText(address);
			((EditText)findViewById(R.id.editText2)).setText(info);
			
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			//rdg.clearCheck();
			
			if(type == R.drawable.male){
				rdg.check(R.id.rdHome);
			}
			else{
				rdg.check(R.id.rdMobile);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			EditText etAddress=(EditText)findViewById(R.id.editText1);
			EditText etInfo =(EditText)findViewById(R.id.editText2);
			
			
			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
			String sEmail = etEmail.getText().toString().trim();
			String sAddress = etAddress.getText().toString().trim();
			String sInfo = etInfo.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sName.length() == 0 || sPhone.length() == 0 || iType == -1 || sInfo.length()==0) {
				Toast t = Toast.makeText(this, 
						"Please fill all the informations", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent data = new Intent();
				data.putExtra("ct_name", sName);
				data.putExtra("ct_phone", sPhone);
				data.putExtra("ct_email", sEmail);
				switch(iType) {
				case R.id.rdHome:
					data.putExtra("ct_type", String.valueOf(R.drawable.male));
					break;
				case R.id.rdMobile:
					data.putExtra("ct_type", String.valueOf(R.drawable.female));
					break;
				}
				data.putExtra("ct_address", sAddress);
				data.putExtra("ct_info", sInfo);
				data.putExtra("id", id);
				setResult(RESULT_OK, data);
				finish();
			}
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}