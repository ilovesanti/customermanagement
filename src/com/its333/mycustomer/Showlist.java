package com.its333.mycustomer;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.net.Uri;
import android.os.Bundle;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Showlist extends ListActivity {
	List<Map<String, String>> list;
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor;
	SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		cursor = getAllContacts();
		adapter = new SimpleCursorAdapter(this, R.layout.activity_showlist,
				cursor, new String[] { "ct_name", "ct_phone", "ct_type",
						"ct_email", "ct_address" }, new int[] { R.id.tvName,
						R.id.tvPhone, R.id.ivPhoneType, R.id.tvEmail }, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}

	private Cursor getAllContacts() {
		return db.query("contacts", new String[] { "_id", "ct_name",
				"ct_phone", "ct_type", "ct_email", "ct_address", "ct_info" },
				null, null, null, null, "ct_name asc");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("ct_name"));
			v.put("ct_phone", data.getStringExtra("ct_phone"));
			v.put("ct_email", data.getStringExtra("ct_email"));
			v.put("ct_type", data.getIntExtra("ct_type", -1));
			v.put("ct_address", data.getStringExtra("ct_address"));
			v.put("ct_info", data.getStringExtra("ct_info"));

			db.insert("contacts", null, v);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		} else if (requestCode == 8888 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("ct_name"));
			v.put("ct_phone", data.getStringExtra("ct_phone"));
			v.put("ct_email", data.getStringExtra("ct_email"));
			v.put("ct_type", data.getIntExtra("ct_type", -1));
			v.put("ct_address", data.getStringExtra("ct_address"));
			v.put("ct_info", data.getStringExtra("ct_info"));
			long id = data.getLongExtra("id", -1);
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) };
			db.update("contacts", v, selection, selectionArgs);
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddcustomerActivity.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo) item.getMenuInfo();
		final long id = a.id; // id of the selected item
		int position = a.position; // position of the selected item
		// Get the selected record
		Cursor c = (Cursor) adapter.getItem(position);
		// Get the values
		String name = c.getString(c.getColumnIndex("ct_name"));
		String phone = c.getString(c.getColumnIndex("ct_phone"));
		String email = c.getString(c.getColumnIndex("ct_email"));
		int itype = c.getInt(c.getColumnIndex("ct_type"));
		String address = c.getString(c.getColumnIndex("ct_address"));
		String info = c.getString(c.getColumnIndex("ct_info"));
		switch (item.getItemId()) {
		case R.id.action_edit:
			// Get the position of the item clicked

			// int pos = a.position;
			// cursor.move(pos);
			// Map<String,String> m =cursor.get(pos);
			// Create an intent to start AddNewActivity
			Intent i = new Intent(this, AddcustomerActivity.class);
			// Attach data with intent
			// i.putExtra("Name",m.get("name"));
			// i.putExtra("Phone",m.get("phone"));
			// i.putExtra("Email",m.get("email"));
			// i.putExtra("Type",m.get("type")); //String value of Image ID
			// i.putExtra("Address",m.get("address"));
			// i.putExtra("Info",m.get("info"));
			// i.putExtra("pos", pos);
			i.putExtra("ct_name", name);
			i.putExtra("ct_phone", phone);
			i.putExtra("ct_email", email);
			i.putExtra("ct_type", itype);
			i.putExtra("ct_address", address);
			i.putExtra("ct_info", info);
			// i.putExtra("pos", pos);
			i.putExtra("id", id);

			// Start activity for result (using different request code)
			startActivityForResult(i, 8888);
			// Toast e = Toast.makeText(this, "Edit menu is selected",
			// Toast.LENGTH_SHORT);
			// e.show();
			return true;
		case R.id.action_del:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			String m = String.format(Locale.getDefault(), "Delete ?");
			builder.setMessage(m);
			builder.setPositiveButton("Yes",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							String selection = "_id = ?";
							String[] selectionArgs = { String.valueOf(id) };
							db.delete("contacts", selection, selectionArgs);
							cursor = getAllContacts();
							adapter.changeCursor(cursor);
							adapter.notifyDataSetChanged();
						}
					});
			builder.setNegativeButton("No", null);
			AlertDialog dialog = builder.create();
			dialog.show();
			// Log.e("delete", "delete");
			// AdapterContextMenuInfo aa =
			// (AdapterContextMenuInfo)item.getMenuInfo();
			// int pospos = aa.position;
			// //System.out.println("delete");
			// cursor.move(pospos);
			// String id = cursor.getString(0);
			// Log.e("delete", id);
			// db.delete("contacts", "_id="+id, null);
			//
			// //list.remove(pospos);
			// adapter.notifyDataSetChanged();

			// Toast d = Toast.makeText(this, "Delete menu is selected",
			// Toast.LENGTH_SHORT);
			// d.show();
			return true;
		case R.id.action_details:
			Intent d = new Intent(this, DetailActivity.class);
			// Attach data with intent
			// i.putExtra("Name",m.get("name"));
			// i.putExtra("Phone",m.get("phone"));
			// i.putExtra("Email",m.get("email"));
			// i.putExtra("Type",m.get("type")); //String value of Image ID
			// i.putExtra("Address",m.get("address"));
			// i.putExtra("Info",m.get("info"));
			// i.putExtra("pos", pos);
			d.putExtra("ct_name", name);
			d.putExtra("ct_phone", phone);
			d.putExtra("ct_email", email);
			d.putExtra("ct_type", itype);
			d.putExtra("ct_address", address);
			d.putExtra("ct_info", info);
			// i.putExtra("pos", pos);
			d.putExtra("id", id);

			// Start activity for result (using different request code)
			startActivityForResult(d, 7777);
			return true;
		case R.id.action_call:
			Intent it = new Intent(Intent.ACTION_CALL);
			it.setData(Uri.parse("tel:"+phone));
			startActivity(it);
		}
		return super.onContextItemSelected(item);
	}
}